import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.css']
})
export class ComponentsComponent implements OnInit {

  valorNumerico:number = 0;

  mensaje:string='';

  
  btndefault(){
    console.log('EL  boton 1,2 y 3 no fueron presionados');
    console.log(this.valorNumerico);
  }


  btn1(){
    console.log('EL  boton 1 fue presionado');
    this.valorNumerico = 1;
    console.log(this.valorNumerico);
    this.mensaje ='BOTON 1';
  }

  btn2(){
    console.log('EL  boton 2 fue presionado');
    this.valorNumerico = 2;
    console.log(this.valorNumerico);
    this.mensaje ='BOTON 2';
  }
  btn3(){
    console.log('EL  boton 3 fue presionado');
    this.valorNumerico = 3;
    console.log(this.valorNumerico);
    this.mensaje ='BOTON 3';
  }


  constructor() {
    this.btndefault();
   }

  ngOnInit(): void {
  }

}
